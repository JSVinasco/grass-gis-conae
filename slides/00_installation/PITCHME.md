---?image=assets/template/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
<h2>Procesamiento y análisis de series temporales en @color[green](GRASS GIS)</h2>
@snapend

@snap[south message-box-white]
<br>Dra. Verónica Andreo<br>CONICET - INMeT<br><br>Instituto Gulich. Córdoba, 2019<br>
@snapend

---?image=assets/template/img/grass.png&position=bottom&size=100% 30%

## Let's install @color[green](GRASS GIS)

[https://grass.osgeo.org/download/software/](https://grass.osgeo.org/download/software/)

![Download software section](assets/img/grass_gis_download_software.png)

---

### @fa[windows] MS Windows users @fa[windows]
<br>
@fa[download] Download the **OSGeo4W installer** from: https://trac.osgeo.org/osgeo4w
<br><br>
> *@size[28px](Important:)*
> @size[28px](Right-click over the installer and execute it with **Administrator** privileges)

+++

@snap[south-west list-content-concise span-100]
@ol[](false)
- Select **Advance install**
- Select **Install from internet**
- Leave the "Install directory" set by default
- Choose *osgeo* server to download software from
- Under *Desktop applications*, select **GRASS GIS stable** and **QGIS desktop**
- Under *Lib*, select **qgis-grass-plugin7**, **matplotlib**, **python-pip**, **python-ply** and **python-pandas** 
- Under *Command line utilities* select **msys**
- Wait for download and installation of packages, and done!
@olend
@snapend

+++?image=assets/template/img/bg/green.jpg&position=left&size=30% 50%

@snap[west text-white]
@size[3em](1.)
@snapend

@snap[east span-70]
Select **Advance install**
<br>
<img src="assets/img/osgeo4w_step_1.png">
@snapend

+++?image=assets/template/img/bg/green.jpg&position=left&size=30% 50%

@snap[west text-white]
@size[3em](2.)
@snapend

@snap[east span-70]
Select **Install from internet**
<br>
<img src="assets/img/osgeo4w_step_2.png">
@snapend

+++?image=assets/template/img/bg/green.jpg&position=left&size=30% 50%

@snap[west text-white]
@size[3em](3.)
@snapend

@snap[east span-70]
Leave the "Install directory" set by default
<br>
<img src="assets/img/osgeo4w_step_3.png">
@snapend

+++?image=assets/template/img/bg/green.jpg&position=left&size=30% 50%

@snap[west text-white]
@size[3em](4.)
@snapend

@snap[east span-70]
Choose *osgeo* server
<br>
<img src="assets/img/osgeo4w_step_4.png">
@snapend

+++?image=assets/template/img/bg/green.jpg&position=left&size=30% 50%

@snap[west text-white]
@size[3em](5.)
@snapend

@snap[east span-70]
Under *Desktop applications*, select **GRASS GIS** and **QGIS desktop**
<br>
<img src="assets/img/osgeo4w_step_5.png">
@snapend

+++?image=assets/template/img/bg/green.jpg&position=left&size=30% 50%

@snap[west text-white]
@size[3em](6.)
@snapend

@snap[east span-70]
Under *Lib*, select **qgis-grass-plugin7**, **matplotlib**, **python-scipy**, **python-pip**, **python-ply** and **python-pandas** 
<br>
<img src="assets/img/osgeo4w_step_6.png">
@snapend

+++?image=assets/template/img/bg/green.jpg&position=left&size=30% 50%

@snap[west text-white]
@size[3em](7.)
@snapend

@snap[east span-70]
Under *Command line utilities* select **msys**
<br>
<img src="assets/img/osgeo4w_step_7.png">
@snapend
<br>

@snap[south span-90]
@size[22px](**Note**: the installer will fetch all other needed dependencies for the applications selected)
@snapend

+++?image=assets/template/img/bg/green.jpg&position=left&size=30% 50%

@snap[west text-white]
@size[3em](8.)
@snapend

@snap[east span-70]
Wait for download and installation, and done @fa[grin-alt]
<br>
![last step](assets/img/osgeo4w_step_10.png)
@snapend

+++?image=assets/template/img/grass.png&position=bottom&size=100% 30%

To update, just open the "OSGeo4W Setup" and start again
<br><br>
**@size[64px](Super easy!)**

---

### @fa[linux] Linux users @fa[linux]
<br>
- **Ubuntu**: Install GRASS GIS from the *unstable* package repository


```bash
sudo add-apt-repository ppa:ubuntugis/ubuntugis-unstable
sudo apt-get update
sudo apt-get install grass grass-dev grass-gui
```

+++

### @fa[linux] Linux users @fa[linux]
<br>
- **Fedora**: Install GRASS GIS from the package manager


```bash
sudo dnf install grass
```
<br>
@size[26px](Other distros: https://grass.osgeo.org/download/software/)

---

### Other software we will use

@ul
- **[pymodis](http://www.pymodis.org/)**: library to work with MODIS data. It offers bulk-download, mosaicking, reprojection, conversion from HDF format and extraction of data quality information. It is needed by *[i.modis](https://grass.osgeo.org/grass74/manuals/addons/i.modis.html)* add-on.

- **[sentinelsat](https://github.com/sentinelsat/sentinelsat)**: utility to search and download Copernicus Sentinel satellite images from the [Copernicus Open Access Hub](https://scihub.copernicus.eu/). It is needed by *[i.sentinel](https://grass.osgeo.org/grass74/manuals/addons/i.sentinel.html)* add-on.

- **[scikit-learn](https://scikit-learn.org/stable/)**: machine learning python package. It is needed by [r.learn.ml](https://grass.osgeo.org/grass7/manuals/addons/r.learn.ml.html) add-on.
@ulend


+++

### Install pymodis, sentinelsat and scikit-learn in Windows @fa[windows]
<br>
- Open the **OSGeo4W shell** and run:


```python
pip install setuptools
pip install pymodis
pip install sentinelsat
pip install scikit-learn
```

+++

### Install pymodis, sentinelsat and scikit-learn in Mac @fa[apple] and Linux @fa[linux]
<br>
- Open a terminal and run 


```python
pip install setuptools
pip install pymodis
pip install sentinelsat
pip install scikit-learn
```

---
@snap[north span-100]
<h3>Data we will use</h3>
@snapend

@snap[south list-content-verbose span-100]
@ul[](false)
- [North Carolina location (150Mb)](https://grass.osgeo.org/sampledata/north_carolina/nc_spm_08_grass7.zip): unzip in `$HOME/grassdata`
- [modis_lst mapset (2Mb)](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/master/data/modis_lst.zip?inline=false): unzip in `$HOME/grassdata/nc_spm_08_grass7`
- [modis_ndvi (15 Mb)](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/master/data/modis_ndvi.zip?inline=false): unzip in `$HOME/grassdata/nc_spm_08_grass7`
- [Sample raster maps (1Mb)](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/3b11ad06d2133889e0ee51652a03f94bfec9d7e4/data/sample_rasters.zip?inline=false): unzip in `$HOME/gisdata`
- [Sample vector map (3Mb)](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/master/data/streets.gpkg?inline=false): move into `$HOME/gisdata`
- [Landsat 8 scenes (14Mb)](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/master/data/NC_L8_scenes.zip?inline=false): unzip in `$HOME/gisdata`
- [Sentinel 2 scene (500Mb)](https://www.dropbox.com/s/2k8wg9i05mqgnf1/S2A_MSIL1C_20180822T155901_N0206_R097_T17SQV_20180822T212023.zip?dl=0): move into `$HOME/gisdata`
- [Aeronet AOD file (12Kb)](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/raw/master/data/180819_180825_EPA-Res_Triangle_Pk.zip?inline=false): unzip in `$HOME/gisdata`
@ulend
@snapend

+++

Folders' structure in `$HOME/grassdata` should be:


```bash
/home/username/grassdata/
├── nc_spm_08_grass7
    ├── landsat
    ├── modis_lst
    ├── modis_ndvi
    ├── PERMANENT
    └── user1
```
<br>

> @size[24px](**Note:** `/home/username/` can be for example the `User/Documents` folder in Windows)

+++

Files in `$HOME/gisdata` folder should be:
- Sample raster maps
- Sample vector map
- L8 scenes
- S2 scene (*DO NOT UNZIP*)
- Aeronet AOD file

---

### One more thing...
<br>
Register yourself at

- NASA services: https://urs.earthdata.nasa.gov/users/new

- ESA-Copernicus services: https://scihub.copernicus.eu/dhus/#/self-registration

+++

Create 2 files, 

`NASA_SETTING.txt` and `SENTINEL_SETTING.txt`, 

with the following content:

```
your_username
your_password
```

and save them under `$HOME/gisdata`


---?image=assets/template/img/grass.png&position=bottom&size=100% 30%

## **We are set, let's start!**

@fa[road fa-4x text-gray]

---

**Thanks for your attention!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
