---?image=assets/template/img/grass.png&position=bottom&size=100% 30%
@title[Front page]

@snap[north span-100]
<br>
<h2>Procesamiento y análisis de series de tiempo en @color[green](GRASS GIS)</h2>
@snapend

@snap[south message-box-white]
<br>Dra. Verónica Andreo<br>CONICET - INMeT<br><br>Instituto Gulich. Córdoba, 2019<br>
@snapend

---?image=assets/template/img/grass.png&position=bottom&size=100% 30%

## Exercise: Create a new Location and Mapset

---

@snap[north-west span-60]
<h3>Overview</h3>
@snapend

@snap[west span-100]
<br><br>
@ol[list-content-verbose]
- Revise GRASS GIS database structure
- Data for the exercise
- Create new locations and mapsets: different options
- Change mapsets / add mapsets to path
- Import raster and vector maps
- Export raster and vector maps
@olend
@snapend

---

@snap[north span-100]
<h3>Data for this exercise</h3>
@snapend

@snap[west span-100]
@ul[list-content-verbose](false)
- Download the [raster](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/blob/master/data/sample_rasters.zip) and [vector](https://gitlab.com/veroandreo/curso-grass-gis-rioiv/blob/master/data/streets.gpkg) sample files
- Create a folder in your `$HOME` directory (or Documents) and name it `gisdata`
- Unzip/Move the files within `$HOME/gisdata`
@ulend
@snapend

---

@snap[north span-100]
<h3>Creating a new Location</h3>
@snapend

@snap[west span-100]
<br>
@ul[](false)
- From the GUI
  @ol[list-content-verbose](false)
  - button "New" in the Location wizard
  - from within a GRASS session: Settings @fa[arrow-right] GRASS working environment @fa[arrow-right] Create new location
  @olend
@ulend
<br><br>
@ul[](false)
- From the command line
  @ol[list-content-verbose](false)
  - using `-c` flag in the *[grass76](https://grass.osgeo.org/grass76/manuals/grass7.html)* start script
  - provide path to new location plus either a georeferenced map or an EPSG code
  @olend
@ulend
@snapend

---

#### Creating a new Location from the GUI

<img src="assets/img/new_location_epsg.png" width="99%">

@size[24px](Create new Lat-Long location using <a href="http://epsg.io/">EPSG</a> code)

---

<h4>Creating new location from command line</h4>

<br>
```bash
# Creates new location with EPSG code 4326
grass76 -c EPSG:4326 $HOME/grassdata/mylocation

# Creates new location based on georeferenced Shapefile 
grass76 -c myvector.shp $HOME/grassdata/mylocation

# Creates new location based on georeferenced GeoTIFF file 
grass76 -c myraster.tif $HOME/grassdata/mylocation
```

@size[26px](This can also be done from a different location; GRASS will switch to the newly created one.)

---

@snap[north span-100]
<h3>Creating a new mapset</h3>
@snapend

@snap[west span-100]
<br>
@ul[](false)
- From the GUI
  @ol[list-content-verbose](false)
  - button "New" in the Mapset wizard
  - from within a GRASS session: Settings @fa[arrow-right] GRASS working environment @fa[arrow-right] Create new mapset
  @olend
@ulend
<br><br>
@ul[](false)
- From command line
  @ol[list-content-verbose](false)
  - with [g.mapset](https://grass.osgeo.org/grass76/manuals/g.mapset.html) command from within a GRASS session
  @olend
@ulend
@snapend

---
@snap[north span-100]
<h4>Creating a new mapset from the GUI</h4>
@snapend

@snap[west span-50]
<br>
@size[24px](using *New* button in wizard)
<img src="assets/img/new_mapset_gui.png" width="95%">
@snapend

@snap[east span-50]
@size[24px](from within a GRASS session)
<img src="assets/img/new_mapset_gui_within_grass.png" width="90%">
@snapend

---

#### Creating a new mapset from command line 

<br>
- Create a mapset from within a running GRASS session:
```bash
# Create a new mapset within a GRASS session
g.mapset -c mapset=curso
```

---

> @fa[tasks] **Tasks:**
>- Create a new location with EPSG:4326 and name it *@color[green](latlong)*
>- Create a new mapset called *@color[green](curso)* within the *latlong* location

<br>

@fa[lightbulb] From command line it's just 2 lines!

---

### Remove Locations or Mapsets
<br>
> Just remove the folder or use the Location wizard

---

### Rename Locations and Mapsets
<br>
> From the Location wizard

---

### Change to a different mapset

- From the GUI:

<img src="assets/img/change_mapset.png" width="60%">

- From command line: 
```bash
# print current mapset
g.mapset -p
# list available mapsets
g.mapsets -l
# change to PERMANENT mapset
g.mapset mapset=PERMANENT
```

---

### Add mapsets to path

Sometimes we need to @color[#8EA33B](*read data from a different mapset*) and use it for a certain processing, so we need to @color[#8EA33B](*see*) that mapset from the current one
<br>
```bash
# check current mapset
g.mapset -p
# print accessible mapsets
g.mapsets -p
# add user1 to the accessible mapsets
g.mapsets mapset=curso operation=add
# check it was added
g.mapsets -p
```

---

### Import raster and vector maps
<br>
- [r.in.gdal](https://grass.osgeo.org/grass76/manuals/r.in.gdal.html): Imports raster data into a GRASS raster map using GDAL library. 
```bash
r.in.gdal input=myraster.tif output=myraster
```
<br>
- [v.in.ogr](https://grass.osgeo.org/grass76/manuals/v.in.ogr.html): Imports vector data into a GRASS vector map using OGR library. 
```bash
v.in.ogr input=myvector.shp output=myvector
```

@size[20px](CRS of maps must match that of the Location)

+++

### Import raster and vector maps
<br>
Alternatively, we can use:

- [r.import](https://grass.osgeo.org/grass76/manuals/r.import.html) 
- [v.import](https://grass.osgeo.org/grass76/manuals/v.import.html)

that offer also re-projection, resampling and subset on the fly @fa[grin-wink]

+++

@snap[north span-100]
<h4>Import a raster map</h4>
@snapend

@snap[west span-50]
<img src="assets/img/r_import_1.png">
@snapend

@snap[east span-50]
<img src="assets/img/r_import_2.png">
@snapend

+++

@snap[north span-100]
<h4>Import a vector map</h4>
@snapend

@snap[west span-50]
<img src="assets/img/v_import_1.png">
@snapend

@snap[east span-50]
<img src="assets/img/v_import_2.png">
@snapend

+++

![imported maps](assets/img/imported_maps.png)

@size[24px](Imported maps are displayed by default)

---

#### Create location and mapset from georeferenced file

<img src="assets/img/new_location_with_file_a.png" width="95%">

+++

#### Create location and mapset from georeferenced file

<img src="assets/img/new_location_with_file_b.png" width="95%">

+++

#### Create location and mapset from georeferenced file

<img src="assets/img/new_location_with_file_8.png" width="80%">

@size[24px](How to get metadata from any raster map?)

```bash
gdalinfo <mapname>
```

---

### Set computational region
<br>
```bash
# check region
g.region -p
# set region to imported raster map
g.region raster=XX
```

---

### Working without importing maps
<br>
We can also only **link** our geodata to the GRASS DB:

- [r.external](https://grass.osgeo.org/grass76/manuals/r.external.html): Links GDAL supported raster data as a pseudo GRASS raster map.
- [v.external](https://grass.osgeo.org/grass76/manuals/v.external.html): Creates a pseudo-vector map as a link to an OGR-supported layer or a PostGIS feature table. 

<br>
@fa[exclamation-triangle] **Do not rename, delete or move the *linked* file afterwards... !**

---

> @fa[tasks] **Tasks:**
>- Create a new location named @color[green](UTM18N) from the L8 band 5 file
>- Change to *latlong* location, mapset curso
>- Import (with reprojection on the fly) the L8 band 2 file into the *latlong* location (mapset curso) and set the computational region to the imported raster map 

---

### Export raster and vector maps
<br>

> @fa[tasks] **Task:**
> Explore [r.out.gdal](https://grass.osgeo.org/grass76/manuals/r.out.gdal.html) and [v.out.ogr](https://grass.osgeo.org/grass76/manuals/v.out.ogr.html) manual pages and export *elevation* and *roadsmajor* maps

---

**Thanks for your attention!!**

![GRASS GIS logo](assets/img/grass_logo_alphab.png)

---

@snap[north span-90]
<br><br><br>
Move on to: 
<br>
[Raster data processing](https://gitpitch.com/veroandreo/grass-gis-conae/master?p=slides/03_raster&grs=gitlab#/)
@snapend

@snap[south span-50]
@size[18px](Presentation powered by)
<br>
<a href="https://gitpitch.com/">
<img src="assets/img/gitpitch_logo.png" width="20%"></a>
@snapend
