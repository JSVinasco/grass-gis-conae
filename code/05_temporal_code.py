#!/usr/bin/env python3

########################################################################
# Script for NDVI time series exercise
# Author: Juan Sebastian Vinasco - Veronica Andre
# Date: October, 2018. Edits: December 2018, April 2019, Mayo 2021
########################################################################

import grass.script as grass
from datetime import time
import grass.temporal as tgis

# TODO create a mapset
# TODO add the modis lst to path
# TODO set the region
# TODO get the boundaries of the region
# TODO download the data
# TODO mode to lat log location
# TODO set region bb suset to region and remove global maps
# TODO reproject
# TODO get familiar with ndvi data
# TODO start
# TODO
# TODO
# TODO
# TODO download modis data


def modis_download_wrapper():
    if not grass.find_program('i.modis.download', '--help'):
        grass.fatal(_("The i.modis.download module was not found, install it first")+
                    "\n" +
                    "g.extension i.modis")

    download_args = {
        'settings' : "$HOME/gisdata/SETTING",
        'product'  : "ndvi_terra_monthly_5600",
        'startday' : "2015-01-01",
        'endday'   : "2017-12-31",
        'folder'   : "$HOME/gisdata/mod13"
    }

    grass.run_command(i.modis.download,
                      **dowload_args)


def main():
    pass





if __name__ == "__main__":
    main()
